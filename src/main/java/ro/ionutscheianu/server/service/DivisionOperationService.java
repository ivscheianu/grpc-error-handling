package ro.ionutscheianu.server.service;

import io.grpc.stub.StreamObserver;
import ro.ionutscheianu.generated.DivisionOperationServiceGrpc;
import ro.ionutscheianu.generated.DivisionRequestOuterClass;
import ro.ionutscheianu.generated.OperationResponseOuterClass;
import ro.ionutscheianu.utils.Logger;

public class DivisionOperationService extends DivisionOperationServiceGrpc.DivisionOperationServiceImplBase {
    @Override
    public void calculate(DivisionRequestOuterClass.DivisionRequest request, StreamObserver<OperationResponseOuterClass.OperationResponse> responseObserver) {
        Logger.log("DivisionOperationService called, " + "args: " + request.getFirstOperand() + ", " + request.getSecondOperand());
        OperationResponseOuterClass.OperationResponse.Builder operationResponseBuilder = OperationResponseOuterClass.OperationResponse.newBuilder();
        if(request.getSecondOperand() == 0){
            throw new IllegalArgumentException("Division by zero");
        }
        int result = request.getFirstOperand() / request.getSecondOperand();
        operationResponseBuilder.setResult(result);
        responseObserver.onNext(operationResponseBuilder.build());
        responseObserver.onCompleted();
    }
}
