package ro.ionutscheianu.server.main;

import io.grpc.ServerBuilder;
import ro.ionutscheianu.server.ExceptionInterceptor;
import ro.ionutscheianu.server.service.DivisionOperationService;
import ro.ionutscheianu.utils.Utils;

import java.io.IOException;
import java.util.Properties;

/**
 * details in README.md
 */


public class Server {

    private io.grpc.Server server;

    public Server() {
        Properties properties = Utils.loadProperties();
        server = ServerBuilder.
                forPort(Integer.parseInt(properties.getProperty("port")))
                .addService(new DivisionOperationService())
                .intercept(new ExceptionInterceptor())
                .build();
    }

    public void start(){
        System.out.println("Starting server...");
        try {
            server.start();
            System.out.println("Server started, port: " + server.getPort());
            server.awaitTermination();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}
