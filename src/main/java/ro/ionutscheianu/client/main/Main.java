package ro.ionutscheianu.client.main;

import ro.ionutscheianu.client.Client;

/**
 * details in README.md
 */


public class Main {
    public static void main(String[] args) {
        Client client = new Client();
        client.start();
    }
}