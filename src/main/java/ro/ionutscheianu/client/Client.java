package ro.ionutscheianu.client;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;
import ro.ionutscheianu.generated.DivisionOperationServiceGrpc;
import ro.ionutscheianu.generated.DivisionRequestOuterClass;
import ro.ionutscheianu.utils.Utils;

import java.util.Properties;
import java.util.Scanner;

/**
 * details in README.md
 */


public class Client {

    private ManagedChannel managedChannel;

    public Client() {
        Properties properties = Utils.loadProperties();
        managedChannel = ManagedChannelBuilder.forAddress(properties.getProperty("ip"), Integer.parseInt(properties.getProperty("port"))).usePlaintext().build();
    }

    public void start() {
        int firstNumber;
        int secondNumber;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Only division accepted!");
        while (true) {
            System.out.println("First number: ");
            firstNumber = scanner.nextInt();
            System.out.println("Second number: ");
            secondNumber = scanner.nextInt();
            getResultFromService(firstNumber, secondNumber);
        }
    }

    private void getResultFromService(int firstNumber, int secondNumber) {
        try {
            int result = callDivisionService(firstNumber, secondNumber);
            System.out.println(firstNumber + " / " + secondNumber + " = " + result);
        } catch (StatusRuntimeException statusRuntimeException) {
            System.out.println(statusRuntimeException.getMessage());
        }
    }

    private int callDivisionService(int firstOperand, int secondOperand) {
        DivisionOperationServiceGrpc.DivisionOperationServiceBlockingStub divisionOperationServiceBlockingStub = DivisionOperationServiceGrpc.newBlockingStub(managedChannel);
        DivisionRequestOuterClass.DivisionRequest divisionRequestRequest = DivisionRequestOuterClass.DivisionRequest
                .newBuilder()
                .setFirstOperand(firstOperand)
                .setSecondOperand(secondOperand)
                .build();
        return divisionOperationServiceBlockingStub.calculate(divisionRequestRequest).getResult();
    }
}
