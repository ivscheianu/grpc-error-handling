package ro.ionutscheianu.generated;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.15.0)",
    comments = "Source: DivisionOperation.proto")
public final class DivisionOperationServiceGrpc {

  private DivisionOperationServiceGrpc() {}

  public static final String SERVICE_NAME = "DivisionOperationService";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<ro.ionutscheianu.generated.DivisionRequestOuterClass.DivisionRequest,
      ro.ionutscheianu.generated.OperationResponseOuterClass.OperationResponse> getCalculateMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "Calculate",
      requestType = ro.ionutscheianu.generated.DivisionRequestOuterClass.DivisionRequest.class,
      responseType = ro.ionutscheianu.generated.OperationResponseOuterClass.OperationResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<ro.ionutscheianu.generated.DivisionRequestOuterClass.DivisionRequest,
      ro.ionutscheianu.generated.OperationResponseOuterClass.OperationResponse> getCalculateMethod() {
    io.grpc.MethodDescriptor<ro.ionutscheianu.generated.DivisionRequestOuterClass.DivisionRequest, ro.ionutscheianu.generated.OperationResponseOuterClass.OperationResponse> getCalculateMethod;
    if ((getCalculateMethod = DivisionOperationServiceGrpc.getCalculateMethod) == null) {
      synchronized (DivisionOperationServiceGrpc.class) {
        if ((getCalculateMethod = DivisionOperationServiceGrpc.getCalculateMethod) == null) {
          DivisionOperationServiceGrpc.getCalculateMethod = getCalculateMethod = 
              io.grpc.MethodDescriptor.<ro.ionutscheianu.generated.DivisionRequestOuterClass.DivisionRequest, ro.ionutscheianu.generated.OperationResponseOuterClass.OperationResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "DivisionOperationService", "Calculate"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  ro.ionutscheianu.generated.DivisionRequestOuterClass.DivisionRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  ro.ionutscheianu.generated.OperationResponseOuterClass.OperationResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new DivisionOperationServiceMethodDescriptorSupplier("Calculate"))
                  .build();
          }
        }
     }
     return getCalculateMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static DivisionOperationServiceStub newStub(io.grpc.Channel channel) {
    return new DivisionOperationServiceStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static DivisionOperationServiceBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new DivisionOperationServiceBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static DivisionOperationServiceFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new DivisionOperationServiceFutureStub(channel);
  }

  /**
   */
  public static abstract class DivisionOperationServiceImplBase implements io.grpc.BindableService {

    /**
     */
    public void calculate(ro.ionutscheianu.generated.DivisionRequestOuterClass.DivisionRequest request,
        io.grpc.stub.StreamObserver<ro.ionutscheianu.generated.OperationResponseOuterClass.OperationResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getCalculateMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getCalculateMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                ro.ionutscheianu.generated.DivisionRequestOuterClass.DivisionRequest,
                ro.ionutscheianu.generated.OperationResponseOuterClass.OperationResponse>(
                  this, METHODID_CALCULATE)))
          .build();
    }
  }

  /**
   */
  public static final class DivisionOperationServiceStub extends io.grpc.stub.AbstractStub<DivisionOperationServiceStub> {
    private DivisionOperationServiceStub(io.grpc.Channel channel) {
      super(channel);
    }

    private DivisionOperationServiceStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected DivisionOperationServiceStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new DivisionOperationServiceStub(channel, callOptions);
    }

    /**
     */
    public void calculate(ro.ionutscheianu.generated.DivisionRequestOuterClass.DivisionRequest request,
        io.grpc.stub.StreamObserver<ro.ionutscheianu.generated.OperationResponseOuterClass.OperationResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getCalculateMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class DivisionOperationServiceBlockingStub extends io.grpc.stub.AbstractStub<DivisionOperationServiceBlockingStub> {
    private DivisionOperationServiceBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private DivisionOperationServiceBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected DivisionOperationServiceBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new DivisionOperationServiceBlockingStub(channel, callOptions);
    }

    /**
     */
    public ro.ionutscheianu.generated.OperationResponseOuterClass.OperationResponse calculate(ro.ionutscheianu.generated.DivisionRequestOuterClass.DivisionRequest request) {
      return blockingUnaryCall(
          getChannel(), getCalculateMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class DivisionOperationServiceFutureStub extends io.grpc.stub.AbstractStub<DivisionOperationServiceFutureStub> {
    private DivisionOperationServiceFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private DivisionOperationServiceFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected DivisionOperationServiceFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new DivisionOperationServiceFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<ro.ionutscheianu.generated.OperationResponseOuterClass.OperationResponse> calculate(
        ro.ionutscheianu.generated.DivisionRequestOuterClass.DivisionRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getCalculateMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_CALCULATE = 0;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final DivisionOperationServiceImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(DivisionOperationServiceImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_CALCULATE:
          serviceImpl.calculate((ro.ionutscheianu.generated.DivisionRequestOuterClass.DivisionRequest) request,
              (io.grpc.stub.StreamObserver<ro.ionutscheianu.generated.OperationResponseOuterClass.OperationResponse>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class DivisionOperationServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    DivisionOperationServiceBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return ro.ionutscheianu.generated.DivisionOperation.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("DivisionOperationService");
    }
  }

  private static final class DivisionOperationServiceFileDescriptorSupplier
      extends DivisionOperationServiceBaseDescriptorSupplier {
    DivisionOperationServiceFileDescriptorSupplier() {}
  }

  private static final class DivisionOperationServiceMethodDescriptorSupplier
      extends DivisionOperationServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    DivisionOperationServiceMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (DivisionOperationServiceGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new DivisionOperationServiceFileDescriptorSupplier())
              .addMethod(getCalculateMethod())
              .build();
        }
      }
    }
    return result;
  }
}
