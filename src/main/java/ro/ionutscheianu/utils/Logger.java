package ro.ionutscheianu.utils;

public final class Logger {

    private Logger(){

    }

    public static void log(String string){
        System.out.println(string);
    }
}
