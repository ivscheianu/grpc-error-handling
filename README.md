Am creat un exemplu simplu, clientul trimite 2 numere, iar serviciul va trebui
sa calculeze rezultatul impartirii celor 2 numere. Astfel am creat "oportunitatea" unei erori runtime:
diviziunea cu 0. Serviciul verifica al doilea argument, iar daca acesta este 0 va arunca o eroare unchecked(runtime): IllegalArgumentException. 
 
 Tot pe partea de server, am creat un interceptor: clasa ExceptionInterceptor care extinde clasa ServerInterceptor.
 Acest interceptor este inregistrat cand construim serverul cu ajutorul unui builder (.intercept(new ExceptionInterceptor())).
 Clasa ExceptionInterceptor suprascrie metoda interceptCall. Tot in clasa ExceptionInterceptor avem un inner class: ExceptionHandlingServerCallListener. Acest inner class este
 instantiat de fiecare data cand metoda interceptCall este apelata.
 In acest inner class am suprascris doar 2 metode onHalfClose() si onReady(), care verifica tipul exceptiei si trimit mai departe o eroare specifica 
 gRPC. Desi exceptia runtime aruncata initial este acoperita de cea gRPC, aceasta poate fi accesata prin metoda .getCause().
 Metodele onCancel() si onComplete() nu au fost suprascrise pentru ca in acest punct al executiei, este prea tarziu.
 
 Pe partea de client, serviciul este apelat intr-un bloc try catch care asteapta exceptii de tipul StatusRuntimeException, exceptii specifice gRPC.
 
 Flow-ul de tratare al exceptiilor gRPC in java cu acest interceptor ma duce cu gandul la design pattern-ul proxy, care este destul de folosit in java. Acesta creeaza un wrapper peste server si intercepteaza actiunile acestuia.
 